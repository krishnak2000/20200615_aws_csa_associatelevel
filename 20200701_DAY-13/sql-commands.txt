CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    column3 datatype,
   ....
);

CREATE TABLE azure.persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);

INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);

INSERT INTO table_name
VALUES (value1, value2, value3, ...);

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0001, 'Rossum', 'Guido', '1-22', 'Hyderabad');

select * from azure.persons;

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0003, 'aws', 'Guido', '1-22', 'AP');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0002, 'azure', 'Guido', '1-22', 'US');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0004, 'bravo', 'Guido', '1-22', 'TS');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0005, 'jerry', 'Guido', '1-22', 'UK');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0007, 'jessi', 'Guido', '1-22', 'Hyderabad');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0006, 'joel', 'Guido', '1-22', 'USA');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0009, 'kesav', 'Guido', '1-22', 'India');




INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0003, 'aws', 'Guido', '1-22', 'AP');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0002, 'azure', 'Guido', '1-22', 'US');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0004, 'bravo', 'Guido', '1-22', 'TS');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0005, 'jerry', 'Guido', '1-22', 'UK');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0007, 'jessi', 'Guido', '1-22', 'Hyderabad');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0006, 'joel', 'Guido', '1-22', 'USA');

INSERT INTO azure.persons (PersonID, LastName, FirstName, Address,City)
VALUES (0009, 'kesav', 'Guido', '1-22', 'India');
